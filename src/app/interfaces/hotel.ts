export interface HOTEL {
  city: string;
  name: string;
  price: string;
  available_on: string;
}
