import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class HotelService {
  constructor(private http: HttpClient) {}

  get() {
    return this.http.get(`${environment.apiUrl}d8c6ab8ac5307d469528`);
  }
}
