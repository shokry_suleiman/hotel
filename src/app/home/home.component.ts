import { HOTEL } from './../interfaces/hotel';
import { HotelService } from './../services/hotel.service';
import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('1s ease-out', style({ opacity: '1' })),
      ]),
      transition(':leave', [
        style({ opacity: '1' }),
        animate('1s ease-out', style({ opacity: '0' })),
      ]),
    ]),
    trigger('fadeIn', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('1s ease-out', style({ opacity: '1' })),
      ]),
    ]),
  ],
})
export class HomeComponent implements OnInit {
  allHotels: HOTEL[];
  hotels: HOTEL[];
  cities: any[];
  citiesChunk: any[];
  hotelsChunk: any[];
  hotelsName: any[];

  searchObj: any = {
    checkIn: null,
    checkOut: null,
  };

  totalNights: number;
  priceLabels: any;

  filterObj: any = {
    price: null,
    cities: [],
    hotels: [],
  };

  hotelsRes: HOTEL[];
  hotelSResChunk: HOTEL[];
  readonly DELIMITER = '-';
  startIndex = 0;

  citiesStartIndx: number = 0;
  hotelsStartIndx: number = 0;

  constructor(private hotelService: HotelService) {}

  ngOnInit(): void {
    this.getAllHotels();
  }

  async getAllHotels() {
    this.hotelService.get().subscribe(async (res: any) => {
      this.allHotels = this.sortLow(res);
      this.hotels = [...this.allHotels];
      this.hotelsRes = [...this.allHotels];

      this.hotelSResChunk = await this.chunkResult(
        this.hotelsRes,
        3,
        this.startIndex
      );

      this.priceLabels = this.getLabelPrices(this.hotelsRes);

      this.cities = await this.getCities(this.hotelsRes);
      this.citiesChunk = this.chunkResultCity(
        this.cities,
        3,
        this.citiesStartIndx
      );

      this.hotelsName = await this.getHotelsName(this.hotelsRes);
      this.hotelsChunk = this.chunkResultHotels(
        this.hotelsName,
        3,
        this.hotelsStartIndx
      );
    });
  }

  async search(data) {
    this.searchObj.checkIn =
      data.from.year +
      this.DELIMITER +
      data.from.month +
      this.DELIMITER +
      data.from.day;
    this.searchObj.checkOut =
      data.to.year +
      this.DELIMITER +
      data.to.month +
      this.DELIMITER +
      data.to.day;

    this.totalNights = await this.calNights(
      this.searchObj.checkIn,
      this.searchObj.checkOut
    );

    this.applySearch(this.searchObj);

    this.hotelsRes = [...this.hotels];

    this.startIndex = 0;
    this.hotelSResChunk = this.chunkResult(this.hotelsRes, 3, this.startIndex);

    this.priceLabels = this.getLabelPrices(this.hotelsRes, this.totalNights);

    this.cities = await this.getCities(this.hotelsRes);
    this.citiesStartIndx = 0;
    this.citiesChunk = this.chunkResultCity(
      this.cities,
      3,
      this.citiesStartIndx
    );

    this.hotelsName = await this.getHotelsName(this.hotelsRes);
    this.hotelsStartIndx = 0;
    this.hotelsChunk = this.chunkResultHotels(
      this.hotelsName,
      3,
      this.hotelsStartIndx
    );
  }

  getCities(hotels: HOTEL[]): any[] {
    let cities: any[] = [];
    hotels.forEach((hotel) => {
      if (cities.map((city) => city.name).indexOf(hotel.city) == -1) {
        cities.push({ name: hotel.city, count: 1 });
      } else if (cities.map((city) => city.name).indexOf(hotel.city) != -1) {
        cities.find((city) => city.name == hotel.city).count += 1;
      }
    });
    return cities;
  }

  getLabelPrices(hotels: HOTEL[], nights?: number): any[] {
    let labelPrices: any[] = [];
    hotels.forEach((hotel) => {
      if (nights) {
        if (labelPrices.indexOf(String(Number(hotel.price) * nights)) == -1)
          labelPrices.push(Number(hotel.price) * nights);
      } else {
        if (labelPrices.indexOf(hotel.price) == -1)
          labelPrices.push(hotel.price);
      }
    });
    return labelPrices;
  }

  getHotelsName(hotels: HOTEL[]): any[] {
    let hotelsName: any[] = [];
    hotels.forEach((hotel) => {
      if (hotelsName.map((hotl) => hotl.name).indexOf(hotel.name) == -1) {
        hotelsName.push({ name: hotel.name, count: 1 });
      } else if (
        hotelsName.map((hotl) => hotl.name).indexOf(hotel.name) != -1
      ) {
        hotelsName.find((hotl) => hotl.name == hotel.name).count += 1;
      }
    });
    return hotelsName;
  }

  sortLow(hotels: HOTEL[]): HOTEL[] {
    let hotls = hotels;
    hotls = hotls.sort((obj1, obj2) => {
      return obj1.price.localeCompare(obj2.price);
    });

    return hotls;
  }

  sortHigh(hotels: HOTEL[]): HOTEL[] {
    let hotls = hotels;
    hotls = hotls.sort((obj1, obj2) => {
      return obj2.price.localeCompare(obj1.price);
    });

    return hotls;
  }

  sortAZ(hotels: HOTEL[]): HOTEL[] {
    let hotls = hotels;
    hotls = hotls.sort((obj1, obj2) => {
      return obj1.name.localeCompare(obj2.name);
    });

    return hotls;
  }

  sortZA(hotels: HOTEL[]): HOTEL[] {
    let hotls = hotels;
    hotls = hotls.sort((obj1, obj2) => {
      return obj2.name.localeCompare(obj1.name);
    });

    return hotls;
  }

  applySearch(data) {
    this.hotels = this.hotels.filter((hotel) => {
      return (
        (data.checkIn.localeCompare(hotel.available_on) == 1 ||
          data.checkIn.localeCompare(hotel.available_on) == 0) &&
        (hotel.available_on.localeCompare(data.checkOut) == -1 ||
          hotel.available_on.localeCompare(data.checkOut) == 0)
      );
    });
    this.hotelsRes = [...this.hotels];
  }

  async applyFilter() {
    this.hotelsRes = [...this.hotels];
    let result = [];
    let temp = [];
    let cityOrhotelfilter = false;
    if (this.filterObj.price != null) {
      if (this.totalNights) {
        this.hotelsRes = this.hotelsRes.filter((hotel) => {
          return (
            this.filterObj.price.low <=
              Number(hotel.price) * this.totalNights &&
            this.filterObj.price.high >= Number(hotel.price) * this.totalNights
          );
        });
      } else {
        this.hotelsRes = this.hotelsRes.filter((hotel) => {
          return (
            this.filterObj.price.low <= hotel.price &&
            this.filterObj.price.high >= hotel.price
          );
        });
      }
    }
    if (this.filterObj.cities.length != 0) {
      cityOrhotelfilter = true;
      temp = this.hotelsRes.filter((hotel) => {
        return (
          this.filterObj.cities.map((city) => city.name).indexOf(hotel.city) !=
            -1 ||
          this.filterObj.cities.map((city) => city.name).indexOf(hotel.city) ==
            0
        );
      });
      result = result.concat(temp);
    }
    if (this.filterObj.hotels.map((hotel) => hotel.name) != 0) {
      cityOrhotelfilter = true;
      temp = this.hotelsRes.filter((hotel) => {
        return (
          this.filterObj.hotels.map((hotl) => hotl.name).indexOf(hotel.name) !=
            -1 ||
          this.filterObj.hotels.map((hotl) => hotl.name).indexOf(hotel.name) ==
            0
        );
      });
      result = result.concat(temp);
    }
    if (cityOrhotelfilter) {
      result = await this.rmRedundent(result);

      this.hotelsRes = [...result];
    }

    this.startIndex = 0;
    this.hotelSResChunk = this.chunkResult(this.hotelsRes, 3, this.startIndex);
  }

  calNights(chIn, chOut) {
    let checkIn = new Date(chIn);
    let checkOut = new Date(chOut);
    let timeDiff = Math.abs(checkOut.getTime() - checkIn.getTime());
    return Math.ceil(timeDiff / (1000 * 3600 * 24));
  }

  async sort(type) {
    switch (type) {
      case 'high': {
        this.hotelsRes = await this.sortHigh(this.hotelsRes);
        this.startIndex = 0;
        this.hotelSResChunk = this.chunkResult(
          this.hotelsRes,
          3,
          this.startIndex
        );

        break;
      }
      case 'low': {
        this.hotelsRes = await this.sortLow(this.hotelsRes);
        this.startIndex = 0;
        this.hotelSResChunk = this.chunkResult(
          this.hotelsRes,
          3,
          this.startIndex
        );

        break;
      }
      case 'az': {
        this.hotelsRes = await this.sortAZ(this.hotelsRes);
        this.startIndex = 0;
        this.hotelSResChunk = this.chunkResult(
          this.hotelsRes,
          3,
          this.startIndex
        );
        break;
      }
      case 'za': {
        this.hotelsRes = await this.sortZA(this.hotelsRes);
        this.startIndex = 0;
        this.hotelSResChunk = this.chunkResult(
          this.hotelsRes,
          3,
          this.startIndex
        );
        break;
      }
      default: {
        break;
      }
    }
  }

  price(event) {
    this.filterObj.price = event;
    this.applyFilter();
  }

  async cityName(event) {
    this.filterObj.cities = await this.checkItemArray(
      this.filterObj.cities,
      event
    );
    this.applyFilter();
  }

  async hotelName(event) {
    this.filterObj.hotels = await this.checkItemArray(
      this.filterObj.hotels,
      event
    );
    this.applyFilter();
  }

  checkItemArray(array: any, item) {
    let arr = array;
    if (arr.map((ar) => ar.name).indexOf(item.name) == -1) {
      arr.push(item);
    } else {
      if (arr.map((ar) => ar.name).indexOf(item.name) == 0) {
        if (arr.length == 1) arr = [];
        else arr.shift();
      } else if (arr.map((ar) => ar.name).indexOf(item.name) == length - 1) {
        arr.pop();
      } else {
        arr.splice(arr.indexOf(item.name), 1);
      }
    }
    return arr;
  }

  chunkResult(array: any[], amount: number, startIndx: number) {
    this.startIndex = this.startIndex + amount;
    if (startIndx + amount > array.length) {
      return array.slice(startIndx, array.length);
    }
    return array.slice(startIndx, amount + startIndx);
  }

  loadMoreRes() {
    this.hotelSResChunk = this.hotelSResChunk.concat(
      this.chunkResult(this.hotelsRes, 3, this.startIndex)
    );
  }

  chunkResultCity(array: any[], amount: number, startIndx: number) {
    this.citiesStartIndx = this.citiesStartIndx + amount;
    if (startIndx + amount > array.length) {
      return array.slice(startIndx, array.length);
    }
    return array.slice(startIndx, amount + startIndx);
  }

  chunkResultHotels(array: any[], amount: number, startIndx: number) {
    this.hotelsStartIndx = this.hotelsStartIndx + amount;
    if (startIndx + amount > array.length) {
      return array.slice(startIndx, array.length);
    }
    return array.slice(startIndx, amount + startIndx);
  }

  loadMoreHotels() {
    this.hotelsChunk = this.hotelsChunk.concat(
      this.chunkResultHotels(this.hotelsName, 3, this.hotelsStartIndx)
    );
  }
  loadMoreCities() {
    this.citiesChunk = this.citiesChunk.concat(
      this.chunkResultCity(this.cities, 3, this.citiesStartIndx)
    );
  }

  rmRedundent(array) {
    const uniqueArray = array.filter((item, index) => {
      const _item = JSON.stringify(item);
      return (
        index ===
        array.findIndex((obj) => {
          return JSON.stringify(obj) === _item;
        })
      );
    });
    return uniqueArray;
  }
}
