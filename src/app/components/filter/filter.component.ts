import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IgxSliderType, TickLabelsOrientation } from 'igniteui-angular';
import { trigger, transition, style, animate } from '@angular/animations';
@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('1s ease-out', style({ opacity: '1' })),
      ]),
      transition(':leave', [
        style({ opacity: '1' }),
        animate('1s ease-out', style({ opacity: '0' })),
      ]),
    ]),
    trigger('fadeIn', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('1s ease-out', style({ opacity: '1' })),
      ]),
    ]),
  ],
})
export class FilterComponent implements OnInit {
  
  @Output() onPrice: EventEmitter<any> = new EventEmitter<any>();
  @Output() onCityName: EventEmitter<any> = new EventEmitter<any>();
  @Output() onHotelName: EventEmitter<any> = new EventEmitter<any>();
  @Output() loadMoreHotels: EventEmitter<any> = new EventEmitter<any>();
  @Output() loadMoreCities: EventEmitter<any> = new EventEmitter<any>();

  @Input() cities: any[];
  @Input() hotels: any[];
  @Input() citiesCount: number;
  @Input() hotelsCount: number;
  @Input() priceLabels;

  public type = IgxSliderType.RANGE;
  public labelsOrientation = TickLabelsOrientation.BottomToTop;
  public priceRange: PriceRange;
  public labels = this.priceLabels;

  ngOnInit(): void {}

  public async change(event) {
    let priceObj = {
      high: this.priceLabels[event.upper],
      low: this.priceLabels[event.lower],
    };
    await this.sleep(1000);

    this.onPrice.emit(priceObj);
  }

  sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  async cityCheck(event, index) {
    await this.sleep(1000);
    this.onCityName.emit(this.cities[index]);
  }

  async hotelCheck(event, index) {
    await this.sleep(1000);
    this.onHotelName.emit(this.hotels[index]);
  }

  loadCities() {
    this.loadMoreCities.emit();
  }

  loadHotels() {
    this.loadMoreHotels.emit();
  }
}

export class PriceRange {
  constructor(public lower: number, public upper: number) {}
}
